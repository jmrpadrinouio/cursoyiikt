<form class="form" method="get" role="form">
    <div class="form-group">
        <label for="a">Valor de A</label>
        <input type="number" name="a">
    </div>
    <div class="form-group">
        <label for="b">Valor de B</label>
        <input type="number" name="b">
    </div>
    <input type="submit" value="Sumar">
    <?php 
    if ( isset( $solucion ) )
    {
        echo '<p class="text-success">' . $solucion . '</p>';
    }
    ?>
</form>