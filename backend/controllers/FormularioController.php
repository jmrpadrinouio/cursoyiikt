<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;

/**
 * Formulario de operaciones controller
 */
class FormularioController extends Controller
{
    public function actionCalcular(){
        if ( !isset( $_GET['a'] ) and !isset( $_GET['b'] ) ){
            return $this->render('formulario');
        }else{
            // hago la suma
            $resultado = $_GET['a'] + $_GET['b'];
            return $this->render('formulario', [
                'solucion' => $resultado,
            ]);
        }
    }
}