<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;

/**
 * Sumar controller
 */
class SumarController extends Controller
{
    public function actionSuma($a,$b){
        if(empty($a) or empty($b)){
            $resultado = 'Debe enviar los valores en los parámetros a y b.';
        }else{
            $resultado = $a + $b;
        }
        return $this->render('resultado',[
            'resultado' => $resultado,
        ]);
    }
}
